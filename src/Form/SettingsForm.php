<?php

namespace Drupal\config_tools\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Serialization\Yaml;
use GitWrapper\GitException;
use GitWrapper\GitWrapper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings form for the Configuration Tools module.
 *
 * @internal
 *   In the future this class may be marked as final.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Constructs a \Drupal\config_tools\Form\SettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('config.factory'));
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['config_tools.config'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_tools_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('config_tools.config');

    $form['disabled'] = [
      '#type' => 'checkbox',
      '#title' => t('Config tools disabled'),
      '#description' => t('Disables config_tools, git_config and files_config.'),
      '#default_value' => $config->get('disabled'),
    ];

    $form['debug'] = [
      '#type' => 'checkbox',
      '#title' => t('Use debug logging'),
      '#description' => t('Logs notices to the Drupal logger.'),
      '#default_value' => $config->get('debug'),
    ];

    $form['directory'] = [
      '#type' => 'textfield',
      '#title' => t('Repository directory'),
      '#description' => t('Provide the absolute path to a directory where the content of the modified configuration object is copied.') . ' ' .
         t('Use a directory outside the Drupal root directory.'),
      '#default_value' => $config->get('directory'),
    ];

    $form['private_key'] = [
      '#type' => 'textfield',
      '#title' => t('Private SSH Key'),
      '#default_value' => $config->get('private_key'),
      '#description' => t('The file path to a private key for this git repository. The private key must be owned by the web server user with execute privileges and located in a directory accessible by the web server user.'),
      '#required' => TRUE,
    ];

    $form['git_username'] = [
      '#type' => 'textfield',
      '#title' => t('Git username'),
      '#default_value' => $config->get('git_username'),
      '#description' => t('User name for communicating with git repo'),
      '#required' => TRUE,
    ];

    $form['git_email'] = [
      '#type' => 'email',
      '#title' => t('Git email'),
      '#default_value' => $config->get('git_email'),
      '#description' => t('Email address for communicating with git repo'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    if (!(\Drupal::service('file_system')->prepareDirectory($form_state->getValue('directory')))) {
      $form_state->setErrorByName('directory', $this->t("The repository directory doesn't exist or is not writable."));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('config_tools.config')
      ->set('disabled', $disabled = $form_state->getValue('disabled'))
      ->set('debugging', $form_state->getValue('debugging'))
      ->set('directory', $repository_directory = $form_state->getValue('directory'))
      ->set('private_key', $private_key = $form_state->getValue('private_key'))
      ->set('git_email', $git_email = $form_state->getValue('git_email'))
      ->set('git_username', $git_username = $form_state->getValue('git_username'))
      ->save();

    if (!$disabled) {
      try {
        $wrapper = new GitWrapper();
        $wrapper->setPrivateKey($private_key);
        $git = $wrapper->workingCopy($repository_directory);

        foreach ($this->configFactory()->listAll() as $config_name) {
          $yml = Yaml::encode($this->configFactory()->get($config_name)->getRawData());
          $filename = $config_name . '.yml';
          file_put_contents($repository_directory . '/' . $filename, $yml);
          $git->config('user.name', $git_username);
          $git->config('user.email', $git_email);
          $git->add($filename);
        }
      }
      catch (GitException $e) {
        watchdog_exception('config_tools', $e);
      }
    }
  }

}
