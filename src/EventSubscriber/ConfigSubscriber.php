<?php

namespace Drupal\config_tools\EventSubscriber;

use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Serialization\Yaml;
use GitWrapper\GitException;
use GitWrapper\GitWrapper;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Saves configuration objects in files.
 *
 * Saves configuration objects in files and remove those files when
 * configuration objects are deleted.
 *
 * @internal
 *   In the future this class may be marked as final.
 */
class ConfigSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[ConfigEvents::SAVE][] = ['onSaveConfig', 10];
    $events[ConfigEvents::DELETE][] = ['onDeleteConfig', 10];
    return $events;
  }

  /**
   * Saves the configuration object in a file.
   *
   * Writes a file with the content of the configuration object that has been
   * altered.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   The configuration event.
   */
  public function onSaveConfig(ConfigCrudEvent $event) {
    $config = \Drupal::config('config_tools.config');
    if (!$config->get('disabled')) {
      $repository_dir = $config->get('directory');
      $saved_config = $event->getConfig();
      if ($saved_config->getOriginal(FALSE) !== $saved_config->getRawData()) {
        if ($repository_dir && \Drupal::service('file_system')->prepareDirectory($repository_dir)) {
          $config_name = $saved_config->getName();
          $git_email = $config->get('git_email');
          $git_username = $config->get('git_username');
          $private_key = $config->get('private_key');
          $yml = Yaml::encode($saved_config->getRawData());
          file_put_contents("$repository_dir/$config_name.yml", $yml);
          if ($config->get('debugging')) {
            \Drupal::logger('config_tools')->notice('The @config_name configuration has been written to @repository_dir', [
              '@config_name' => $config_name,
              '@repository_dir' => $repository_dir,
            ]);
          }
          if ($private_key && $git_username && $git_email) {
            try {
              $wrapper = new GitWrapper();
              $wrapper->setPrivateKey($private_key);
              $git = $wrapper->workingCopy($repository_dir);
              $git->config('user.name', $git_username);
              $git->config('user.email', $git_email);
              $git->add("$config_name.yml");
              if ($config->get('debugging')) {
                \Drupal::logger('config_tools')->notice('The @config_name configuration has been updated.', ['@config_name' => $config_name]);
              }
            }
            catch (GitException $e) {
              watchdog_exception('config_tools', $e);
            }
          }
        }
      }
    }
  }

  /**
   * Deletes the file storing the copy of the delete configuration object.
   *
   * Deletes the file containing a copy of the configuration object that has
   * been deleted.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   The configuration event.
   */
  public function onDeleteConfig(ConfigCrudEvent $event) {
    $config = \Drupal::config('config_tools.config');
    if (!$config->get('disabled')) {
      if ($repository_dir = $config->get('directory')) {
        $deleted_config = $event->getConfig();
        $config_name = $deleted_config->getName();
        unlink("$repository_dir/$config_name.yml");
        if ($config->get('debugging')) {
          \Drupal::logger('config_tools')->notice('The @config_name configuration has been removed from @repository_dir', [
            '@config_name' => $config_name,
            '@repository_dir' => $repository_dir,
          ]);
        }
        $git_username = $config->get('git_username');
        $git_email = $config->get('git_email');
        if ($private_key = $config->get('private_key')) {
          try {
            $wrapper = new GitWrapper();
            $wrapper->setPrivateKey($private_key);
            $git = $wrapper->workingCopy($repository_dir);
            $git->config('user.name', $git_username);
            $git->config('user.email', $git_email);
            $git->rm("$config_name.yml");
            if ($config->get('debugging')) {
              \Drupal::logger('config_tools')->notice('The @config_name configuration has been removed.', ['@config_name' => $config_name]);
            }
          }
          catch (GitException $e) {
            watchdog_exception('config_tools', $e);
          }
        }
      }
    }
  }

}
