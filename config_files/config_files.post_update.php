<?php

/**
 * @file
 * Contains post-update functions for the Configuration Files module.
 */

use Drupal\Core\Extension\MissingDependencyException;
use Drupal\Core\Utility\UpdateException;

/**
 * Installs the Config Tools module.
 */
function config_files_post_update_001_install_config_tools() {
  // The Config Tools module should have been declared as dependency of the
  // Configuration Files module. It is now installed from this post-update hook.
  // Uninstalling the Config Tools module is not allowed from the
  // ConfigFilesUninstallValidator class.
  try {
    \Drupal::service('module_installer')->install(['config_tools'], FALSE);
  }
  catch (MissingDependencyException $e) {
    throw new UpdateException('The Configuration Tools module is a dependency of the Configuration Files module, and it needs to be installed.');
  }
}

/**
 * Merge the config_files.config configuration into config_tools.config.
 */
function config_files_post_update_002_merge_configuration() {
  /** var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
  $config_factory = Drupal::configFactory();
  /** var \Drupal\Core\Config\ImmutableConfig $files_config */
  $files_config = $config_factory->get('config_files.config');
  /** var \Drupal\Core\Config\Config $tools_config */
  $tools_config = $config_factory->getEditable('config_tools.config');

  $tools_config->set('directory', $files_config->get('directory'))
    ->save();
}
