<?php

namespace Drupal\config_files;

use Drupal\Core\Extension\ModuleUninstallValidatorInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Prevents the Configuration Tools module from being uninstalled.
 */
class ConfigFilesUninstallValidator implements ModuleUninstallValidatorInterface {
  use StringTranslationTrait;

  /**
   * Constructs a ConfigFilesUninstallValidator instance.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(TranslationInterface $string_translation) {
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public function validate($module) {
    $reasons = [];
    if ($module == 'config_tools') {
      $reasons[] = $this->t('To uninstall the Configuration Tools module, first uninstall the Configuration Files module');
    }

    return $reasons;
  }

}
