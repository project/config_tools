<?php

/**
 * @file
 * Contains installation hooks for the Configuration Tools module.
 */

use GitWrapper\GitWrapper;
use GitWrapper\GitException;

/**
 * Implements hook_requirements().
 */
function config_tools_requirements($phase) {
  $requirements = [];

  if (class_exists(GitWrapper::class)) {
    try {
      $git_wrapper = new GitWrapper();
    }
    catch (GitException $gitException) {
      $requirements['git_executable']['title'] = t('Git executable');
      $requirements['git_executable']['value'] = t('Not found');
      $requirements['git_executable']['description'] = t(
        "The %class class isn't able to find the Git executable, which is required from the %module module.",
        ['%class' => GitWrapper::class, '%module' => 'Configuration Tools']
      );
      $requirements['git_executable']['severity'] = REQUIREMENT_ERROR;

      return $requirements;
    }

    $requirements['git_wrapper_library']['title'] = t('%library library', ['%library' => 'cpliakas/git-wrapper']);
    $requirements['git_wrapper_library']['value'] = $git_wrapper->version();
    $requirements['git_wrapper_library']['description'] = t(
      "The required %library library has been installed and it was able to find the Git executable required from the %module module.",
      ['%library' => 'cpliakas/git-wrapper', '%module' => 'Configuration Tools']
    );
    $requirements['git_executable']['severity'] = REQUIREMENT_OK;
  }
  else {
    $requirements['git_wrapper_library']['title'] = t('%library library', ['%library' => 'cpliakas/git-wrapper']);
    $requirements['git_wrapper_library']['value'] = t('Not found');
    $requirements['git_wrapper_library']['description'] = t(
      'The <a href=":git_link">%library</a> library is required from the %module module.',
      [
        ':git_link' => 'https://github.com/cpliakas/git-wrapper',
        '%library' => 'cpliakas/git-wrapper',
        '%module' => 'Configuration Tools',
      ]
    );
    $requirements['git_wrapper_library']['severity'] = REQUIREMENT_ERROR;
  }

  return $requirements;
}

/**
 * Clear caches.
 */
function config_tools_update_8100() {
  // This update is intentionally empty as its purpose is only updating the
  // schema number and causing a cache rebuild.
  // Using hook_update_N() to clear the cache should generally be avoided; see
  // https://www.drupal.org/node/2960601 for more details.
}
