<?php

/**
 * @file
 * Contains post-update functions for the Configuration Tools module.
 */

/**
 * Delete the config_files_write state value.
 */
function config_tools_post_update_001_delete_config_files_write() {
  \Drupal::state()->delete('config_files_write');
}
