<?php

/**
 * @file
 * Contains post-update functions for the Git Configuration module.
 */

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;

/**
 * Merge the git_config.config configuration into config_tools.config.
 */
function git_config_post_update_001_merge_configuration() {
  /** var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
  $config_factory = Drupal::configFactory();
  /** var \Drupal\Core\Config\ImmutableConfig $git_config */
  $git_config = $config_factory->get('git_config.config');
  /** var \Drupal\Core\Config\Config $tools_config */
  $tools_config = $config_factory->getEditable('config_tools.config');

  $tools_config->set('git_email', $git_config->get('git_email'))
    ->set('git_username', $git_config->get('git_username'))
    ->set('private_key', $git_config->get('private_key'))
    ->save();
}
